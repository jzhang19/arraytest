# Overview

This workspace leverages Gitpod's standard Docker Image Workspace-Full and the Docker Compose YAML file to configure the application's connection to the database and the database admin UI. The application has three APIs for the user to register, login, and logout.

## Table of Contents
* [API](#apis)
* [Design](#design)
* [Testing](#testing)

## APIs
	Methods		Urls				Actions
	POST		/api/user/signup	Signup a new account
	POST		/api/user/login		Login an account and generate a web based token in Cookies
	POST		/api/user/logout	Logout the account and removes the token from Cookies

## Design

* It uses spring initializr to configure a Java spring boot maven project using Java 17 and Spring Boot 3.0.0(M4). Spring Boot makes it easier to configure a production-ready spring application and its Spring Web dependency speeds up the process to develop Restful APIs. Java 17 is the last LTS release which provides new features over the older versions. Since this is a new web application in a docker container, using the last LTS release keeps the technology up to date.  
	```
	* Dependencies
		|_____ Spring Web (Using it for restful apis)
		|_____ Spring Security (Using it for access-control)
		|_____ Spring Data JPA (Using it to interact with the relational DB)
		|_____ PostgreSQL Driver (Postgres)
		|_____ Jwt Web Token (Using it to generate time-based token stored in the Cookies for future login)
	```
* Choosing Postgres because the web site will have more reads than writes since the write happens when the users register or update the password; whereas, users login to the website constantly. 
	* SQL script
	```
		CREATE TABLE IF NOT EXISTS users (
			id serial PRIMARY KEY,
			username VARCHAR (50) UNIQUE NOT NULL, 
			password VARCHAR (120) NOT NULL,
			email VARCHAR (355) UNIQUE NOT NULL,
			created_on TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
			last_login TIMESTAMP
		); 
	```

* Access PgAdmin Postgres Database
	1. Click on the remote explorer and open the port 5050. It redirects you to the PgAdmin Login. Here is the credential username/passowrd: admin@admin.com/root
	2. Connect to Server with password: postgres
		- Troubleshoot: If you are not able to connect, right click on the "postgres" server and then click on the "Properties" optoin. In the "Connection" tab, change the Host name/address to 172.18.0.2 or 172.18.0.3 and then save it. Try to connect to the server again with the password.
	3. You can there is a "users" table in the Tables.

* Project Structure
```
	|__src
	|___|__main
	|   |____|__ controllers (Rest APIs)
	|   |____|__ exceptions (Custom exceptions)
	|   |____|__ models
	|   |____|__ payload (Requests and Responses Models)
	|   |____|__ repository
	|   |____|__ security (Security configuration and JWT token generator)
	|   |____|__ services (Implementations)
    |	|____|__ TestApplication.java (Spring Boot main function)
	|	|__resources
	|	|	|__ application.properties (Config for database connection, spring hibernate config, jwt token.) 
	|	|__tests
	|__ gitpod.yml (Defines the public Docker image specified in gitpod.dockerfile and tells Gitpod how to prepare & build the project)
	|__ gitpod.dockerfile 
	|__ docker-compose.yml (Sets up the postgres db connection and the pgAdmin page to visualize the data)
```

## Testing
* Start the Spring boot application
	1. Find TestApplication.java (search using ctrl + p or in folder src/main/java/com/array/test/TestApplication.java) and then click run
	2. In remote explorer, check Port 8080 is green and public

* Please download the Array Test.postman_collection and then import into your postman. You can find the postman collection in the repository. It contains three posts:
	* Replace postman request urls to this format: https://8080-{shared repo snapshot url}/api/user/{path}
		- https://8080-white-silverfish-6mtj00y6cun.ws-us63.gitpod.io/api/user/signup
		- https://8080-white-silverfish-6mtj00y6cun.ws-us63.gitpod.io/api/user/login
		- https://8080-white-silverfish-6mtj00y6cun.ws-us63.gitpod.io/api/user/logout
	* Signup
		- Method: POST
		- Body: 
		```
		{
			"username": "alan1",
			"password": "123456",
			"email": "usalanzhang1@gmail.com"
		}
		```
		- Response: 
			- 200 if success
			- 400 bad request if username or email is already taken (Although the requirement uses email as username but I'm still collecting username)
		
	* Login
		- Method: POST
		- Body: 
		```
		{
			"email": "usalanzhang1@gmail.com",
			"password": "123456"
		}
		```
		- Response: 
			- 200 if success (Cookies set as ArrayTest:Value)
			- 403 if login is not successful

	* Logout
		- Method: POST
		- Response:
			- 200 if success (Cookies set to ArrayTest:Null)
