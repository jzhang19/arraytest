FROM gitpod/workspace-full

USER gitpod

RUN bash -c ". /home/gitpod/.sdkman/bin/sdkman-init.sh && \
    sdk install java 17.0.3-ms && \
    sdk default java 17.0.3-ms"
    
# RUN sudo service postgresql start \
#     && until pg_isready; do sleep 1; done \
#     # Create the PostgreSQL user. 
#     # Hack with double sudo is because gitpod user cannot run command on behalf of postgres user.
#     && sudo sudo -u postgres psql \
#         -c "CREATE USER gitpod PASSWORD 'gitpod' SUPERUSER" \
#         -c "CREATE DATABASE gitpod OWNER gitpod"
