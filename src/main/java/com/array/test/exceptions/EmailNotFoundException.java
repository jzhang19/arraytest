package com.array.test.exceptions;

public class EmailNotFoundException extends Exception {
  
    public EmailNotFoundException(String msg) {
        super(msg);
    }
}
