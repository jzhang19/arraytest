package com.array.test.security;

import java.security.Key;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Base64;
import java.util.Date;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import javax.crypto.spec.SecretKeySpec;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseCookie;
import org.springframework.stereotype.Component;
import org.springframework.web.util.WebUtils;

import com.array.test.services.UserDetailsImpl;

import io.jsonwebtoken.*;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;


@Component
public class JwtUtils {

    @Value("${jwt.name}")
    private String jwtName;

    @Value("${jwt.secret}")
    private String jwtSecret;
  
    @Value("${jwt.expMs}")
    private Long jwtExpMs;

    public ResponseCookie generateJwtCookie(UserDetailsImpl userDetailImpl) {
        String token = createToken(userDetailImpl.getEmail(), userDetailImpl.getUsername());
        ResponseCookie cookie = ResponseCookie.from(jwtName, token).maxAge(TimeUnit.MILLISECONDS.toSeconds(jwtExpMs)).httpOnly(true).build();
        return cookie;
    }

    public ResponseCookie removeJwtCookie() {
        ResponseCookie cookie = ResponseCookie.from(jwtName, null).build();
        return cookie;
    }

    public String getJwtCookie(HttpServletRequest request) {
        Cookie cookie = WebUtils.getCookie(request, jwtName);
        if (cookie != null) {
            return cookie.getValue();
        } else {
            return null;
        }
    }

    private Key generateKey(){
        Key hmacKey = new SecretKeySpec(Base64.getDecoder().decode(jwtSecret), SignatureAlgorithm.HS512.getJcaName());
        return hmacKey;
    }

    private String createToken(String email, String username) {
        Key hmacKey = generateKey();
        Instant now = Instant.now();
        
        return Jwts.builder()
            .claim("email", email)
            .setSubject(username)
            .setId(UUID.randomUUID().toString())
            .setIssuedAt(Date.from(now))
            .setExpiration(Date.from(now.plus(jwtExpMs, ChronoUnit.MILLIS)))
            .signWith(hmacKey)
            .compact();
    }
}
